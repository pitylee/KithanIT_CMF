<?php
include(getcwd()."index.php");
if(file_exists(getcwd()."/components/admin/dashboard/controller.php")){include(getcwd()."/components/admin/dashboard/controller.php");}
if(file_exists(getcwd()."/components/admin/dashboard/model.php")){include(getcwd()."/components/admin/dashboard/model.php");}

use PHPUnit\Framework\TestCase;
class DashboardControllerTest extends TestCase
{
    protected $stack;

    protected function setUp()
    {
        $this->stack = [];
    }

    public function testEmpty()
    {
        $this->assertTrue(empty($this->stack));
    }

    public function testbuild()
    {
        // Arrange
        $a = new DashboardController();

        // Act
        $b = $a->build();

        // Assert
        // $this->assertTrue( $b );
        // $this->expectOutputString("");
        //         $this->assertContains('foo', 'FooBar', '', true);
        // $this->assertDirectoryExists('/path/to/directory');
        // $this->assertDirectoryIsReadable('/path/to/directory');
        $this->assertNull($b);

        // $this->assertFalse(is_callable(array($a, 'build')));

        $this->assertTrue(is_callable(array($a, 'build')));        
    }

    // ...
}
