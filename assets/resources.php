<?php  
    require_once $_SERVER['DOCUMENT_ROOT'].'/assets/config.php';
    
    $builder = new Builder();
    $resources = new Resources();
    
    // This is the key part, we call a method that has a name stored in $stored
    Resources::init();
    
    $libraryClassName = Resources::getClassName();
    $libraryClass = Resources::create($libraryClassName);
    if( class_exists($libraryClassName) &&  method_exists($libraryClass,"debug")  && Router::get("debug")  )
        $libraryClass::debug();
    
    ob_end_flush();
    Query::disconnect();
?>

