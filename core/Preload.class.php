<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Preload
 *
 * @author Pityu
 */
class Preload {
    
    public function __construct(){
    }
    
    public static function collection($preload=null){
        if( ALLOW_PRELOAD != true ) return;
        
        // if the session is not encrypted yet do so
        $_SESSION["preload"] = ( isset($_SESSION["preload"]) ) ? $_SESSION["preload"] : encrypt(array(), SALT);
        $template = Template::parse();
        
        if( !Core::isDev() ){
            if( isset( $template["preload"] ) )
                return $template["preload"];
        }

        if( $preload ){
            $spreload = decrypt($_SESSION["preload"], SALT);
            $spreload = unserialize($spreload);
            
            $serializedPreload = array_replace_recursive( $spreload , $preload );
           
            $template_json = Builder::$template_path . "template.json";
            $template = ( fileexists($template_json) ) ? json_decode( file_get_contents($template_json), true ) : array();

            if( is_array($template) ){
                $templatePreload = array_replace_recursive($template, array("preload"=>$serializedPreload));
                $templatePreload = json_encode($templatePreload, JSON_PRETTY_PRINT);
                writefile($template_json, $templatePreload);
            }

            $serializedPreload = encrypt($serializedPreload, SALT);
            $_SESSION["preload"] = $serializedPreload;
             
        }
        else{
            $preload = decrypt($_SESSION["preload"], SALT);
            $preload = unserialize($preload);

            if( empty($preload) ){
                $template_json = Builder::$template_path . "template.json";
                $template = ( fileexists($template_json) ) ? json_decode( file_get_contents($template_json), true ) : null;
                $preload = ( isset($template["preload"]) ) ? $template["preload"] : null;
            }

            return $preload;
        }
    }
}
