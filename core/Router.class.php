<?php

/**
 * Router is responsible for handling paths in our application.
 * It determines which controller to load, which action to invoke and what
 * are the arguments. It is capable of building paths that you can later use to
 * create links.
 */
class Router
{
	static $param, $path, $route, $chunks, $defaultController, $defaultAction, $var_to_pass;
        static $lang, $controller, $action, $arguments, $layout, $view_path, $domain;
	
	public function __construct($path=null)
	{
            self::$domain = domain();
            self::$param = self::url("param");
            // We define the chunks from the url for pre-use
            self::$chunks = explode("/", self::$param);
            self::$chunks = array_values( array_filter(self::$chunks) );
            // We set the url param chunks into our variables, until it will be set to their permanent values
            self::$lang = ( Core::getSettings("site_ismultilingual") == "yes" && isset(self::$chunks[0]) && strlen(self::$chunks[0]) <= 2 ) ? array_shift(self::$chunks) : Core::getSettings("default_page_lang");

            self::$layout = ( isset(self::$chunks[0]) && Layout::layoutExists(self::$chunks[0]) ) ? array_shift(self::$chunks) : null;

            // if we have an alias layout we must unset first the first element (contrller) from the chunks, or else the controller will be alias...
            if( isset(self::$chunks[0]) && Layout::layoutAlias(self::$chunks[0]) ){ array_shift(self::$chunks); }
            
            // don't let the cache ruin our life, this is not java
            if( isset(self::$chunks[0]) && self::$chunks[0] !="cache" ) self::$controller = array_shift(self::$chunks);
            else self::$controller = null;
            self::$action = ( in_array(self::$controller,Builder::$libraries) ) ? self::$controller ."/". array_shift(self::$chunks) : null;

            self::$arguments = self::$chunks;
            if( isset(self::$arguments[0]) && self::$arguments[0] == Template::current() && array_has(self::$arguments,"uploads") ) { 
                array_shift(self::$arguments);
                if( self::$arguments[0] == "uploads" ) $uploads=true; array_shift(self::$arguments);
                array_unshift(self::$arguments,Template::current()) ;
                if( isset($uploads) && $uploads==true ) array_unshift(self::$arguments,"uploads");
            }
            self::$arguments = implode("/", self::$arguments);

            if(strlen(self::$controller) < 1)
                    self::$controller = "/" . self::$defaultController;

            if(strlen(self::$action) < 1)
                    self::$action = "/". self::$controller ."/". self::$defaultAction;
            
            // Check in database for custom route
            self::$param = ( self::getCustomRoute(self::$param) ) ? self::getCustomRoute(self::$param) : self::$param;
            
            // Check in database for translation of controller and action
            if( self::getCustomRoute(self::$action) )
                self::$param = self::customizeRoute(self::getCustomRoute(self::$action), "action");
            elseif( self::getCustomRoute(self::$controller) )
                self::$param = self::customizeRoute(self::getCustomRoute(self::$controller), "controller");
                
            self::$path = ( $path ) ? $path : self::$param;
            
	}
        
    
    public static function getCustomRoute($route){
        $query = Core::query("SELECT * FROM `routes` WHERE `from`='".$route."'");
        $result = Query::getResults();
        $result = ( isset($result["to"]) ) ? $result["to"] : null;
        return $result;
    }
    
    public static function customizeRoute($route, $type){
        $currentpath = "/" . self::url("param");
        
        $croute = str_replace(self::$$type, $route, $currentpath);
        
        return $croute;
    }
        
	public static function setDefaultController($name)
	{
		self::$defaultController = $name;
	}
	
	public static function setDefaultAction($name)
	{
		self::$defaultAction = $name;
	}
	public static function getLang()
	{
		return strtolower(self::$lang);
	}
	public static function getController()
	{
            return self::$controller;
	}
        
	public static function getAction()
	{
		return self::$action;
	}
	
	public static function getArguments()
	{
		return self::$arguments;
	}
	
	public static function getArgument($index)
	{
            if(isset(self::$arguments[$index]))
                return self::$arguments[$index];
            else
                return null;
	}
	
	// This method splits the path into chunks separated by / character.
	// First chunk is a controller name, second one is an action name,
	// the rest are arguments which are later passed to the controller
	public static function parse()
	{
            self::$view_path = Builder::$components . self::getController() . "/";
            self::$path = (self::$path) ? self::$path : "";
            self::$path = (has("/",self::$path)) ? self::$path : "/".self::$path;

            self::$chunks = explode('/', self::$path);
            self::$chunks = array_values( array_filter(self::$chunks) );

            $skip=array("cache","rsrc","resources");
            self::$lang = ( Core::getSettings("site_ismultilingual") == "yes" && isset(self::$chunks[0]) && strlen(self::$chunks[0]) <= 2 ) ? array_shift(self::$chunks) : Core::getSettings("default_page_lang");
            // default controller if only one sub, or language + sub

            Builder::$layout = ( isset(self::$chunks[0]) && Layout::layoutExists(self::$chunks[0]) ) ? self::$chunks[0] : null; 
            Builder::$layout = ( !Builder::$layout && isset(self::$chunks[1]) && Layout::layoutExists(self::$chunks[1]) ) ? self::$chunks[1] : Builder::$layout;        
            Builder::$layout = ( !Builder::$layout && isset(self::$chunks[2]) && Layout::layoutExists(self::$chunks[2]) ) ? self::$chunks[2] : Builder::$layout;        
            Builder::$layout = ( !Builder::$layout && isset(self::$chunks[3]) && Layout::layoutExists(self::$chunks[3]) ) ? self::$chunks[3] : Builder::$layout;        

			self::$layout = ( isset(self::$chunks[0]) && Layout::layoutExists(self::$chunks[0]) ) ? array_shift(self::$chunks) : null;

            // if we have an alias in config json... 
			if( !self::$layout && isset(self::$chunks[0]) && Layout::layoutAlias(self::$chunks[0]) ){
                self::$layout = Layout::layoutAlias(self::$chunks[0]);
                array_shift(self::$chunks);
            }
            // self::$layout = ( !self::$layout && ) ? array_shift(self::$chunks) : null;

            self::$controller = array_shift(self::$chunks);

            self::$action =  ( count(self::$chunks) >= 1 ) ? array_shift(self::$chunks) : "index";

            if(strlen(self::$controller) < 1)
                self::$controller = self::$defaultController;
                
            if(strlen(self::$action) < 1)
                    self::$action = self::$defaultAction;

            if( self::$controller == "cache" || self::$controller == "rsrc" ){

                $cfile = explode('/',self::getArguments());
                if(  array_has($cfile,"uploads") ) { 
                    $temp = $cfile[0];
                    $cfile[0] = $cfile[1];
                    $cfile[1] = $temp;
                }
                $file = self::getArguments();
                $cfile=implode("/",$cfile);
                $cfile=str_replace(array("//","cache"),"",$cfile);
                $cachefile = Builder::$cache_path . "/" . $cfile;

                $rfile = explode('/',self::getArguments());
                if( isset( $rfile[0] ) && $rfile[0]=="cache" ) array_shift($rfile);
                if( isset( $rfile[0] ) && $rfile[0]=="resize" ) $library = array_shift($rfile);
                if(  array_has($rfile,"uploads") ) { 
                    $temp = $rfile[0];
                    $rfile[0] = $rfile[1];
                    $rfile[1] = $temp;
                }
                $library = (!isset( $library )) ? $rfile[0] : $library;

                $rfile=implode("/",$rfile);
                $rfile = str_replace(array("cache"),"",$rfile);
                $realfileInfo = pathinfo($rfile);
                $realpath = Builder::$root . "/" . $realfileInfo["dirname"] . "/" . $realfileInfo["basename"];
                $file = array_filter(explode("/",$file));
                if( isset($file[0]) &&  in_array($file[0],Builder::$libraries) || $file[0] == "cache" ) array_shift($file);
                if( isset($file[0]) &&  in_array($file[0],Builder::$libraries) || $file[0] == "cache" ) array_shift($file);
                $file = implode("/",$file);
                $tfile = str_replace(array("resize","cache"),"",$file);
                $realpath = (fileexists($realpath)) ? $realpath :  Builder::$templates_dir ."/" . $tfile;
                $fileE = explode("/", $file);
                $file = fixpath($file);
                $cachefile = fixpath($cachefile);
                $realuri = ( $fileE[0] == $library ) ? Builder::$resources_uri.$file : Builder::$resources_uri.$library."/".$file;
                
                if( !fileexists($cachefile) && fileexists($realpath) ){
                    redirectURL($realuri);
                }
                else{
                    if( Router::get("debug") ){
                        echo "Fullpath: " . $realpath;
                        dump( fileexists($realpath) );
                        echo "<br/>";
                        echo "Cache file: " . $cachefile;
                        dump( fileexists($cachefile) );
                    }
                    Layout::generateError("404");
                }
            }
            
            self::$arguments = ( count(self::$chunks) > 0 ) ? self::$chunks : array();
            
	}

    public static function isBot(){
        $browser = self::userAgent("userAgent");
        $return = ( self::get("bot") ) ? true : false;
        
        if( has($browser,"bot") ) $return = true;
        if( has($browser,"crawler") ) $return = true;
        
        return $return;
    }
    
    public static function userAgent($return=null){
        // userAgent name version platform pattern
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'Windows';
            if (preg_match('/NT 6.2/i', $u_agent)) { $platform .= ' 8'; }
                elseif (preg_match('/NT 6.3/i', $u_agent)) { $platform .= ' 8.1'; }
                elseif (preg_match('/NT 6.1/i', $u_agent)) { $platform .= ' 7'; }
                elseif (preg_match('/NT 6.0/i', $u_agent)) { $platform .= ' Vista'; }
                elseif (preg_match('/NT 5.1/i', $u_agent)) { $platform .= ' XP'; }
                elseif (preg_match('/NT 5.0/i', $u_agent)) { $platform .= ' 2000'; }
            if (preg_match('/WOW64/i', $u_agent) || preg_match('/x64/i', $u_agent)) { $platform .= ' (x64)'; }
        }
        
        
        $ub=""; $bname="";
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
        { 
            $bname = 'Internet Explorer'; 
            $ub = "MSIE"; 
        } 
        elseif(preg_match('/Firefox/i',$u_agent)) 
        { 
            $bname = 'Mozilla Firefox'; 
            $ub = "Firefox"; 
        } 
        elseif(preg_match('/Chrome/i',$u_agent)) 
        { 
            $bname = 'Google Chrome'; 
            $ub = "Chrome"; 
        } 
        elseif(preg_match('/Safari/i',$u_agent)) 
        { 
            $bname = 'Apple Safari'; 
            $ub = "Safari"; 
        } 
        elseif(preg_match('/Opera/i',$u_agent)) 
        { 
            $bname = 'Opera'; 
            $ub = "Opera"; 
        } 
        elseif(preg_match('/Netscape/i',$u_agent)) 
        { 
            $bname = 'Netscape'; 
            $ub = "Netscape"; 
        } 

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
        }
        else {
            $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        $useragent = array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
        
        
        if( $return && isset($useragent[$return])) $info = $useragent[$return];
        else $info = $useragent;
        
        return $info;
    }      
	public static function buildGets()
	{
            $uri = self::url("full");
            
            if ( strpos($uri,"?") === false ) return;
                    
            $gets = explodeit("?", $uri, 1);
            $gets = explode("&", $gets);
            
            foreach($gets as $get){
                $get = explode("=", $get);
                $variable = $get[0];
                $value = isset($get[1]) ? $get[1] : "";
                
                $_GET[$variable] = $value;
            }
            
            // unset($_GET["param"]);
	}
	
        public static function inject(){
            $params = func_get_args ();
           
            // if we receive array we work'em all
            if( is_array($params[0]) ){
                $params = $params[0];
            }
            else{
                $params = array(
                    $params[0] => $params[1]
                );
            }
            $variable = key($params);
            
            self::$var_to_pass[$variable] = $params[$variable];
        }
	
	public static function url($what="param", $replace=null)
        {
            // $what = strtolower($what);
            $protocol = explode("/", $_SERVER["SERVER_PROTOCOL"])[1];
            $domain = $_SERVER["SERVER_NAME"];
            $param = ( isset($_GET["param"]) ) ? $_GET["param"] : "/";
            
            if( $what == "chunk" )
                $toshow = strtolower($protocol . "://" . $domain);
            elseif( $what == "url" )
                $toshow = $protocol . "://" . $domain . $param;
            elseif( $what == "https" )
                $toshow = "https://" . $domain . $param;
            elseif( $what == "http" )
                $toshow = "http://" . $domain . $param;
            elseif( $what == "full" )
                $toshow = $_SERVER["REQUEST_URI"];
            else
                $toshow = $param;

            return ($toshow);
	}
        
        public static function get($get){
            self::buildGets();
            
            if( isset($_GET[$get]) )
                return true;
            else 
                return false;
        }
        
}

?>