var gulp = require('gulp');
var tinyLr = require('tiny-lr');
var open = require('gulp-open');
var config = require("./config.json");

/*
<script src="/resources/node_modules/livereload-js/dist/livereload.js?debug&host=192.168.1.100&port=35729"></script>
add to above:
url = url.replace("/rsrc/", "/resources/"); 
line: 1075
*/ 
var paths = [
    './*.php',
    './.htaccess',
    './assets/*.php',
    './components/**/*.*',
    './core/*.*',
    './core/*/*.*',
    './core/*/**/*.*',
    './layout/*.*',
    './layout/**/*.*',
    './libraries/*.*',
    './libraries/**/*.*',
    './templates/*/*.*',
    './templates/*/*/*.*',
    './uploads/**/*.*',
    '!./**/gen_preload.js',
    '!./**/template.json'
];

var url = ( config.url != "" && config.url ) ? config.url : "localhost";

gulp.task('browser', function(){
    var options = {
        uri: url,
        app: 'google-chrome'
    };
    gulp.src(__filename)
    .pipe(open(options));
});

gulp.task('watch', function () {
    var liveReload = tinyLr();
    liveReload.listen(35729);
    gulp.watch(paths, function (watchEvent) {
        console.log(watchEvent);
        liveReload.changed({
            body: {
                files: [watchEvent.path]
            }
        });
    });
});

gulp.task('open', ['browser', 'watch']);