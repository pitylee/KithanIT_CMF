
  <div id="page-content" class="home-slider-content">
    <div class="container">
      <div class="home-with-slide">
        <div class="row">

          <div class="col-md-9 col-md-push-3">
            <div class="page-content">
	
<?php View::render(); ?>

              <div class="change-view">
                <div class="filter-input">
                  <input type="text" placeholder="Filter by Keywords">
                </div>
              </div> <!-- end .change-view -->


              <div class="product-details">
                <div class="tab-content">

                  <div class="tab-pane active" id="all-categories">
                    <h3>Globo <span>Categories</span></h3>

                    <div class="row clearfix">
                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-university"></i>Antiques</a>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-car"></i>Cars &amp; Motorcycles</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-laptop"></i>Computers &amp; Tablets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->


                  <div class="tab-pane" id="advertisemnet">
                    <h3>Advertisement <span class="comments">59</span></h3>

                    <div class="row clearfix">

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-car"></i>Cars &amp; Motorcycles</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-university"></i>Antiques</a>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-laptop"></i>Computers &amp; Tablets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="e-commerce">
                    <h3>E-Commerce <span class="comments">99</span></h3>

                    <div class="row clearfix">


                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="education">
                    <h3>Education <span class="comments">79</span></h3>

                    <div class="row clearfix">
                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-university"></i>Antiques</a>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>


                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="entertainment">
                    <h3>Entertainment <span class="comments">45</span></h3>

                    <div class="row clearfix">


                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="home-garder">
                    <h3>Home &amp; Garden <span class="comments">78</span></h3>

                    <div class="row clearfix">
                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-university"></i>Antiques</a>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-car"></i>Cars &amp; Motorcycles</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-laptop"></i>Computers &amp; Tablets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="industry">
                    <h3>Industry <span class="comments">33</span></h3>

                    <div class="row clearfix">

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="#"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="libraries-public">
                    <h3>Libraries &amp; Public <span class="comments">66</span></h3>

                    <div class="row clearfix">


                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-car"></i>Cars &amp; Motorcycles</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-laptop"></i>Computers &amp; Tablets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="#"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="real-estate">
                    <h3>Real Estate <span class="comments">55</span></h3>

                    <div class="row clearfix">
                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-university"></i>Antiques</a>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>


                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->

                  <div class="tab-pane" id="resturants">
                    <h3>Reasturants &amp; Pubs <span class="comments">66</span></h3>

                    <div class="row clearfix">
                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-university"></i>Antiques</a>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-book"></i>Book</a>
                        </div>

                      </div>


                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paint-brush"></i>Creative &amp; Digital</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-female"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-bicycle"></i>Fashion</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-archive"></i>Furnishing</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-eye"></i>Health &amp; beauty</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-paper-plane-o"></i>Hobbies</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-empire"></i>Jewelry</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-cutlery"></i>Kitchen</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-sun-o"></i>Leisure</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-music"></i>Music</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-money"></i>Tickets</a>
                        </div>

                      </div>

                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="category-item">

                          <a href="search-result.html"><i class="fa fa-rocket"></i>Video Games &amp; Consoles</a>
                        </div>

                      </div>

                      <div class="view-more">
                        <a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>View More</a>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->
                </div> <!-- end .tabe-content -->

              </div> <!-- end .product-details -->
            </div> <!-- end .page-content -->
          </div>

          <div class="col-md-3 col-md-pull-9 category-toggle">
            <button><i class="fa fa-briefcase"></i></button>

            <div class="page-sidebar">
              <div class="custom-search">

                <div class="location-details">
                  <form action="#">
                    <div class="select-country">
                      <label>Country</label>

                      <select class="" data-placeholder="-Select-">
                        <option value="option1">option 1</option>
                        <option value="option2">option 2</option>
                        <option value="option3">option 3</option>
                        <option value="option4">option 4</option>
                      </select>

                    </div> <!-- end .select-country -->

                    <div class="select-state">
                      <label>State</label>

                      <select class="" data-placeholder="-Select-">
                        <option value="option1">option 1</option>
                        <option value="option2">option 2</option>
                        <option value="option3">option 3</option>
                        <option value="option4">option 4</option>
                      </select>

                    </div> <!-- end .select-state -->

                    <div class="zip-code">
                      <label>ZIP Code</label>

                      <input type="text" placeholder="Enter">

                    </div> <!-- end .zip-code -->
                  </form>

                </div> <!-- end .location-details -->

                <div class="distance-range">
                  <p>
                    <label for="amount">Distance</label>
                    <input type="text" id="amount">
                  </p>

                  <div id="slider-range-min"></div>
                </div>  <!-- end #distance-range -->
              </div> <!-- end .custom-search -->

              <!-- Category accordion -->
              <div id="categories">
                <div class="accordion">
                  <ul class="nav nav-tabs home-tab" role="tablist">
                    <li class="active">
                      <a href="#all-categories"  role="tab" data-toggle="tab">All Categories
                        <span>Display all sub-categories</span>
                      </a>
                    </li>

                    <li>
                      <a href="#advertisemnet" role="tab" data-toggle="tab">advertisement
                        <span>Agencies, Marketing</span>
                      </a>
                    </li>

                    <li>
                      <a href="#e-commerce"  role="tab" data-toggle="tab">E-commerce
                        <span>Boutiques, Shop</span>
                      </a>
                    </li>

                    <li>
                      <a href="#education" role="tab" data-toggle="tab">Education
                        <span>Private School, Universities</span>
                      </a>
                    </li>

                    <li>
                      <a href="#entertainment" role="tab" data-toggle="tab">Entertainment
                        <span>Sport, Toys, Travels</span>
                      </a>
                    </li>

                    <li>
                      <a href="#home-garder" role="tab" data-toggle="tab">Home &amp; Garden
                        <span>Accessories, Furniture, Plants</span>
                      </a>
                    </li>

                    <li>
                      <a href="#industry" role="tab" data-toggle="tab">Industry
                        <span>Accessories, Products, Services</span>
                      </a>
                    </li>

                    <li>
                      <a href="#libraries-public" role="tab" data-toggle="tab">Libraries &amp; Public
                        <span>Libraries, Postal, Public Offices</span>
                      </a>
                    </li>

                    <li>
                      <a href="#real-estate" role="tab" data-toggle="tab">Real Estate
                        <span>Apartments, Commercial, House</span>
                      </a>
                    </li>

                    <li>
                      <a href="#resturants" role="tab" data-toggle="tab">Resturants &amp; Pubs
                        <span>Bars, Fast Food, Resturants</span>
                      </a>
                    </li>

                    <li>
                      <a href="#" role="tab" data-toggle="tab"><i class="fa fa-angle-right"></i>See More</a>

                    </li>

                  </ul>
                </div> <!-- end .accordion -->
              </div> <!-- end #categories -->

            </div> <!-- end .page-sidebar -->
          </div> <!-- end grid layout-->
        </div> <!-- end .row -->
      </div> <!-- end .home-with-slide -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->
  