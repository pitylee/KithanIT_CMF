<?php

use Patchwork\JSqueeze;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Less
 *
 * @author Pityu
 */

class_alias("JsLibrary", "Js");

class JsLibrary {
    static $current, $cachefile, $cachemodifiedtime;
    
    public function __construct() {
        if( !Router::get("debug") )
            error_reporting(0);
	self::$cachefile = Resources::$cachefile;
	self::$cachemodifiedtime = Resources::$cachemodifiedtime;
    }
    
    public static function build(){
        self::parse();
    }
    

    public static function parse($file=null){
        $template = Builder::$template_path;
        $template = Builder::$template_path;
        $library = Resources::$library;
        $filename = Resources::$file;
        $folders = Resources::getFolders();
        $folders = str_replace(Template::current()."/","",$folders);
        $file = ( !fileexists($template . "/". $folders . $filename) ) ? Builder::$root."/".  $folders . $filename: $template . "/". $folders . $filename;

        $javascript = Resources::$template["javascript"][0];
        $version = $javascript["jQuery_version"];
        $jqueryfile = $template . $library ."/jquery-".$version.".js";
        $jqueryurl = "http://code.jquery.com/jquery-".$version.".js";
        
        $tempcache = "";

        
        // if( $filename == "jquery-settings.js" && Core::isDev() ){
        if( $filename == "jquery-settings.js" && Core::isDev() && Router::get("dev") ){
            $tempcache = "// Cached file : " . $filename . " from ". date("Y-m-d H:i:s", cache_modtime(self::$cachefile)) ." \n\n";
            
            $tempcache .= file_get_contents($file);
            $tempcache = self::inject($tempcache);
            return $tempcache;
        }

        
        
        if( Core::isDev() || !cache_exists(self::$cachefile) || filesize(self::$cachefile) <= 10 || self::$cachemodifiedtime > Core::getSettings("cache_timeout") ){
            if( $filename != "js" && fileexists($file) && $filename != "jquery.js" ){
                $tempcache .= file_get_contents($file);
                $tempcache .= "\n\n";
            
                if( $filename == "jquery-settings.js" ){
                    $tempcache = self::inject($tempcache);
                }
            }
            elseif( $filename == "jquery.js" ){
                // Include jQuery or download from the site if there is
                if( fileexists($jqueryfile) ){
                            // If exists on server
                            $tempcache .= file_get_contents($jqueryfile);
                            $tempcache .= "\n\n";
                            $jquery = true;
                }
                elseif( remote_fileexists($jqueryurl) === true ){
                    // If does not exists and found on jQuery repository
                    $tempcache .= Builder::download($jqueryurl, $jqueryfile);
                    $tempcache .= "\n\n";
                }
                else{
                    Resources::build("html");
                    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                    echo "<h1>404 Not Found</h1>";
                    echo "The page that you have requested could not be found.<br/><br/>";
                    
                    // Not found and could not be downloaded show comment and log to console
                    $content = "<i>jQuery file not found: v".$version." nor could be downloaded from the jQuery site: \n " .$jqueryurl . "</i>";
                    $content .= "<script type=\"text/javascript\">console.log(\"jQuery version ".$version." not found nor could be downloaded from the jQuery site \");</script>";

                    echo $content;
                    
                    die;
                }
                
            }
            else{
                Resources::build("html");
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                echo "<h1>404 Not Found</h1>";
                echo "The page that you have requested could not be found.";
                die;
            }
            
            $tempcache = self::minify($tempcache);
            
            cache_write(self::$cachefile, $tempcache, "w");
        }
        else{
            $tempcache = file_get_contents(self::$cachefile);
        }

        echo $tempcache;
        
        if( !has(self::$current, "gen") )
            return $tempcache;
    }
    
    public static function minify($minifiedCode)
    {
        $jz = new JSqueeze();

        $minifiedCode = $jz->squeeze(
            $minifiedCode,
            true,   // $singleLine
            false,   // $keepImportantComments
            false   // $specialVarRx
        );
        /*
        $minifiedCode = \JShrink\Minifier::minify($minifiedCode, array('flaggedComments' => false));
        */
        
        return $minifiedCode;
    }    
    
    public static function inject($content){
        $template = Resources::$template;
        
        if( $template && isset($template["variables"]) ){
            $temptemplate = $template["variables"][0];
            foreach( $temptemplate as $key=>$value ){
                    // Check if hexa color, and add a # if it has not
                    if( has($key, "color") && is_hex($value) ){
                            if( $value[0] != "#" ){
                                $temptemplate[$key]= "#" . $value;
                            }
                    }
            }
            
            $template = array_merge ($template, $temptemplate);
        }
        extract($template);
        
        $pattern = '/\{\$(.*)\}/i';
        preg_match_all($pattern, $content, $matches);
        
        if( !empty($matches[0]) && !empty($matches[1]) ){
            foreach( $matches as $key=>$value ){
                if( isset($matches[1][$key]) && isset($matches[0][$key]) ){
                    $theMatchedString = $matches[0][$key];
                    $variable = $matches[1][$key];
                    $variable = isset($$variable) ? $$variable : "notset";
                    $content = str_replace($theMatchedString, $variable, $content);
                }
            }
        }
        return $content;
    }
    
    
    public static function debug(){
        $arguments = ( Router::getArguments() ) ? Router::getArguments() : array();
        echo "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\"  style=\"width: 450px; border: 1px solid black;\">

            <tr> <td colspan=\"2\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-weight:bold;\"> <a target=\"_blank\" href=\"" . Router::url("url") . "\"> URL </a> </td> </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Path:</td>
                <td height=\"30\" valign=\"middle\">" . Router::url("path") . "</td>
            </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Library:</td>
                <td height=\"30\" valign=\"middle\">" . Resources::getClassName(Resources::getLibrary()) . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Folders:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFolders() . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">File:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFile() . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Extension:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("extension") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Created:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("created") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Gets:</td>
                <td height=\"30\" valign=\"middle\">" . str_replace( ";", "<br/>", rarray( $_GET ) ) . "</td>
            </tr>
        </table>";
    }
}

?>