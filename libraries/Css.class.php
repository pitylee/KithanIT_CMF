<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Css
 *
 * @author Pityu
 */

class_alias("CssLibrary", "Css");

class CssLibrary {
    static $less, $cachefile, $cachemodifiedtime;
    
    public function __construct(){
        if( !Router::get("debug") )
            error_reporting(0);

	self::$cachefile = Resources::$cachefile;
	self::$cachemodifiedtime = Resources::$cachemodifiedtime;
        self::$less = new lessc();
    }
    
    public function build(){

        echo self::parse();
    }
    
    public static function parse($file=null){
        $template = Builder::$template_path;
        $library = Resources::$library;
        $filename = Resources::$file;
        $folders = Resources::getFolders();
        $folders = str_replace(Template::current()."/","",$folders);
        if( fileexists(Builder::$root ."/". $folders . $filename) ){ $file = Builder::$root . "/". $folders . $filename; }
        else{ $file = ( $file ) ? $file : $template . "/". $folders . $filename; }
        $tempcache = "";
        
        if( Core::isDev() || !cache_exists(self::$cachefile) || self::$cachemodifiedtime > Core::getSettings("cache_timeout") ){
            if( fileexists($file) ){
                $tempcache .= file_get_contents($file);
                $tempcache .= "\n\n";

                // remove comments
                $regex = "!/\*[^*]*\*+([^/][^*]*\*+)*/!";
                $tempcache = preg_replace($regex,"",$tempcache);
                
                // remove tabs, consecutivee spaces, newlines, etc.
                $tempcache = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '	', '	'), '', $tempcache);
                // remove single spaces
                $tempcache = str_replace(array(" {", "{ ", "; ", ": ", " :", " ,", ", ", ";}"), array("{", "{", ";", ":", ":", ",", ",", "}"), $tempcache);
        
                $tempcache = LessLibrary::fixpaths($tempcache);
        
                // Write into cache
                cache_write( self::$cachefile, $tempcache );
            }
            else{
                Resources::build("html");
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                echo "<h1>404 Not Found</h1>";
                echo "The page that you have requested could not be found.";
                die;
            }
        }
        else{
            $tempcache = file_get_contents(self::$cachefile);
        }
        
        if( Core::isDev() && Router::get("dev") ){                
            $tempcache = "// Cached file : " . $filename ." from " . date("Y-m-d H:i:s", cache_modtime(self::$cachefile)) . " \n\n";
            $tempcache .= file_get_contents($file);
            $tempcache = LessLibrary::fixpaths($tempcache);
        }
        
        Resources::build("css");
        echo $tempcache;
        
    }
    

    public static function debug(){
        $arguments = ( Router::getArguments() ) ? Router::getArguments() : array();
        echo "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\"  style=\"width: 450px; border: 1px solid black;\">

            <tr> <td colspan=\"2\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-weight:bold;\"> <a target=\"_blank\" href=\"" . Router::url("url") . "\"> URL </a> </td> </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Path:</td>
                <td height=\"30\" valign=\"middle\">" . Router::url("path") . "</td>
            </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Library:</td>
                <td height=\"30\" valign=\"middle\">" . Resources::getClassName(Resources::getLibrary()) . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Folders:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFolders() . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">File:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFile() . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Extension:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("extension") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Created:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("created") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Gets:</td>
                <td height=\"30\" valign=\"middle\">" . str_replace( ";", "<br/>", rarray( $_GET ) ) . "</td>
            </tr>
        </table>";
    }
}
